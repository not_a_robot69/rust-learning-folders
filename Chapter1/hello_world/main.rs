// main function is executed before all other functions
// all of its code is between curly brackets
fn main(){
  println!("Hello world!");
  // excalamation mark in 'println!' shows that we're calling a rust *macro*. It would be a
  // function instead if it was called as 'println' (no '!').
  // Like C, expressions end with ';' to indicate that the next one is ready to begin.
}
