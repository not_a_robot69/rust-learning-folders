// similar to '#include <stdio.h>' in C, the keyword 'use' uses a the library it is given. The io
// (input/output) library comes with the standard library (std). This includes 'io' from 'std'.
use std::io;
// use the 'Rng' trait, that defines methods that random number genarators implement
use rand::Rng;
// 'Ordering' is another enum, like 'Result', but the variants are 'Less', 'Greater' & 'Equal'; the
// 3 possible outcomes when comparing 2 values.
use std::cmp::Ordering;

// main function. 'fn' declares new function, arguments in brackets and body of function in curly
// brackets.
fn main(){
  // 'println!' prints to screen, as explained in previous chapter.
  println!("Guess the number!");

  // function 'rand::thread_rng' is a rand num gen (RNG) that is local to current thread of
  // execution and is seeded by OS. The method 'gen_range' called on the RNG takes 2 nums as args
  // and generates a rand num between them (lower bound inclusive, upper bound exclusive). This
  // method is defined by the 'Rng' trait brought into scope with 'use rand::Rng' earlier.
  let secret_number = rand::thread_rng().gen_range(1, 101);

  loop{
    println!("Please input your guess.");

    // 'let' statement is used to declare variable. Usual syntax is:
    // let foo = bar;
    // but here it is a mutable variable because of the keyword 'mut'. Mutability is discussed in
    // Chapter 3. Variables are immutable by default in Rust.
    // the '=' sign binds the variable 'guess' to a 'String', which is a string type provided by
    // the standard library, that is a growable, UTF-8 encoded bit of text.
    // The '::' here indicates that 'new' is an *associated function* of the 'String' type. An
    // associated function is implemented on type, rather than on a particular instance of a
    // 'String'.
    // This 'new function creates a new, empty string. The 'new' function is found on many types,
    // because it is a common name for a function that makes a new valu of some kind.
    let mut guess = String::new();
  
    // here, we call the '.read_line' method from the 'stdin' function, that is in the 'io'
    // library. Had we not wrote 'use std::io' at the beginning, we could've called this by writing
    // 'std::io:stdin()'. The 'stdin' function returns an instance of 'std::io::Stdin', which is a
    // type that represents a handle to the standard input for the terminal.
    // the next part of the code calls the 'read_line' method on the stdi handle to get input from
    // the user. It is given the arguments '&mut guess'.
    // what 'read_line' does is take user input into standard input and places it into a string, so
    // it takes that string as an argument. The string argument needs to be mutable so the method
    // can change the string's content by adding user input.
    // the '&' shows that the argument is a *reference* (appears to be similar to pointers in C).
    // like variables, references are immutable by default, so you need to write '&mut guess' to
    // indicate it is mutable.
    // 'read_line' returns a value, which is an 'io:Result' in this case. There are different types
    // of 'Result' in the std library: a generic 'Result' as well as specific versions for
    // submodules, such as 'io::Result'. 'Result' types are enumeration, or enums.
    // For 'Result', the variants are 'Ok' or 'Err'. 'Ok' indicates operation was successful, and
    // inside 'Ok' is the successfully generated value. 'Err' means operation failed, and contains
    // information on how/why it failed.
    // The purpose of 'Result' types is error-handling. Values of 'Result', like all values, have
    // methods defined on them. An instance of 'io::Result' has an 'expect' method that can be
    // called. If 'io::Result' is an 'Err', then 'expect' will crash program and display its
    // argument as a message. If it is 'Ok', then 'expect' will take 'Ok's return value and return
    // that value, which is the number of bytes of the user input in this case.
    io::stdin()
      .read_line(&mut guess)
      .expect("Failed to read line");
  
    // Rust allows us to shadow previous 'guess' with a new one. Often used when converting a value
    // from one type to another (like rn). Shadowing lets us reuse 'guess' var name instead of using
    // 2 unique vars.
    // 'guess' after '=' is original 'guess', 'trim' method eliminates any whitespace at
    // begininning/end (\n, ' ', \t, etc.).  Although 'u32' type can only take nums, user must press
    // enter (\n) to satisfy 'read_line', that 'trim' eliminates.
    // 'parse' method parses string into several num types, so we need to specify, with the 'u32'
    // after the ':' after 'guess' signifying it is unsigned 32-bit num. Rust will infer that
    // 'secret_number' is also 'u32', because it has type inference.
    // 'parse' can fail if input is not a number, so, like 'read_line' it returns a 'Result', which
    // will now be handled with a 'match' expression for proper error handling.
    // 'match' will compare 'guess.trim().parse()' with its arms.
    let guess:u32 = match guess.trim().parse(){
        // If 'parse' returned 'Ok', then 'match' will return the num value contained in 'Ok' to 'guess' var.
        Ok(num) => num,
        // If 'parse' returned 'Err' value with more info about error, then it will be matched
        // against '_', a catchall value. The program will 'continue' (return to beginning of loop)
        Err(_) => continue,
    };
  
    // '{}' are placeholders for arguments at the end. (similar to format specifiers)
    println!("You guessed: {}", guess);
  
    // 'cmp' method compares 2 values, and takes a reference to whatever you want to compare with.
    // Then, returns variant of 'Ordering' enum brought into scope with 'use'. 'match' expression
    // used to decide what to do next based on which variant of 'Ordering' was returned from 'cmp'.
    // 'match' expression is made up of /arms/; arm consists of a /pattern/ & code to execute if
    // value given at beginning of 'match' = arm's pattern. Rust takes value given to 'match' and
    // looks through each arm in turn.
    match guess.cmp(&secret_number){
      Ordering::Less => println!("Too small!"),
      Ordering::Greater => println!("Too big!"),
      Ordering::Equal => {
        println!("You win!"); 
        break;
      }
    }
  }
}
