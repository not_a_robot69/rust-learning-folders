use std::io;

fn main(){
  println!("This program converts Fahernheit to Celsius.");
  println!("Input the number to convert:");

  let mut num = String::new();
  io::stdin().read_line(&mut num).expect("[ERROR] failed to read line");

  let num:f64 = num.trim().parse().expect("[ERROR] not a valid number");

  let celsius = ((num - 32.0)*5.0)/9.0;
  println!("Your temperature in Celsius: {}°C.", celsius);
  println!("Note: because of computers' binary system, decimal numbers are displayed really weirdly.");
}
