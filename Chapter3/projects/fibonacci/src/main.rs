use std::io;

fn main(){
  println!("This program will generate the nth Fibonacci number, where n is your input\nInput a number:");

  let mut n = String::new();
  io::stdin().read_line(&mut n).expect("[ERROR] failed to read line");

  let n:u32 = n.trim().parse().expect("[ERROR] not a valid number");

  let mut first = 1;
  let mut second = 1;
  let mut fibo = 1;

  for _i in 2..n{
    fibo = first + second;
    first = second;
    second = fibo;
  }

  println!("The {}{} Fibonacci number is: {}", n, if n == 1{
    "st"
  } else if n == 2{
    "nd"
  } else{
      "th"
  }, fibo)
}
