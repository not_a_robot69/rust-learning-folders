#[derive(Debug)] // adding debug trait so we can print it later
struct Rectangle{
  width:u32,
  height:u32,
}

fn main() {
  let rect1 = Rectangle{
    width:30,
    height:50,
  };

  println!("rect1 is {:?}", rect1);

  println!("The area of the rectangle is {} square pixels.", area(&rect1));
}

fn area(rectangle:&Rectangle) -> u32{
  rectangle.width * rectangle.height
}

// this program is a lot clearer and manageable since it is indexing names instead of numbers.
// however, the area function is specific to the Rectangle struct, so it would be helpful if it was
// tied to it in some way. We can refactor this code to use a /method/ instead of a function...
