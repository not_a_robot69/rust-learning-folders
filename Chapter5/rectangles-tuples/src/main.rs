fn main() {
  let rect1 = (30, 50);

  println!("The area of the rectangle is {} square pixels.", area(rect1));
}

fn area(dimensions:(u32, u32)) -> u32{
  dimensions.0 * dimensions.1
}

// while this program is better, since the width and height are linked together, it has also become
// less clear: we have to index parts of a tuple instead of using names. For this particular case,
// it doesn't matter if we mix the width and height values, but in other cases, such as printing
// the rectangle on a screen, it would obviously matter a lot. Overall this program gives a lot of
// room for error.
