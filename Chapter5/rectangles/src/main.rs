fn main() {
  let width1 = 30;
  let height1 = 50;

  println!("The area of the rectangle is {} square pixels.", area(width1, height1));
}

fn area(width:u32, height:u32) -> u32{
  width * height
}

// the problem with this program is that it's calculating the area of 1 rectangle, and takes width
// and height as parameters, and while those 2 values are related, this isn't expressed anywhere in
// this program. It'd be more readable and manageable if these 2 values were grouped together.
// We can do this using tuples...
